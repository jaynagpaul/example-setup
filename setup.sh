#!/usr/bin/env bash

PLACEHOLDERS=("YOUR_POSTGRES_DB_NAME"
              "YOUR_POSTGRES_USER" \
              "YOUR_POSTGRES_PASSWORD" \
              "YOUR_GITHUB_OAUTH_KEY" \
              "YOUR_GITHUB_OAUTH_SECRET" \
              "YOUR_GITLAB_OAUTH_KEY" \
              "YOUR_GITLAB_OAUTH_SECRET" \
              "YOUR_GITHUB_WEBHOOK_SECRET" \
              "YOUR_RABBIT_USER" \
              "YOUR_RABBIT_PASSWORD" \
              "YOUR_DJANGO_SECRET_KEY" \
              "your.domain.tld" \
	      "/path/to/ssl.crt" \
	      "/path/to/ssl.key")

files=$(find . -path ./.git -prune -o -name "setup.sh" -prune -o -type f -print)

for placeholder in ${PLACEHOLDERS[@]}; do
    read -p "Replace placeholder $placeholder to? " value

    for file in $files; do
        sed -i -e "s#$placeholder#$value#" $file
    done
done
